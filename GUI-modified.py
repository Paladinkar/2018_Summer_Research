import tkinter as tk
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from tkinter import ttk
import time as clock
import random as rm
import numpy as np
from math import *
ran = rm.random
x=0
y=0
Refac=3
Activ=2 # set time for refractory, active
func = [0 for i in range(30)]
func2 = [0 for i in range(30)]
class Node:
    def __init__(self,x,y):
        self.Current = 0
        self.RefactoryTime=0
        self.ActiveTime=0
        self.Neighbors=[]
        self.state = "."
        self.posx =x
        self.posy = y

def ReAct(Node): #Change of state and cool-down periods
    if (Node.state=="*"):
        if (Node.RefactoryTime==0):
            Node.state="."
            Node.Current=0
        else:
            Node.RefactoryTime -=1



    elif (Node.state=="."):
        if (Node.Current>3):
            Node.state="$"
            Node.ActiveTime=Activ
            Node.Current-=1



    elif (Node.state=="$"):
        if(Node.ActiveTime==0):
            Node.state="*"
            Node.RefactoryTime=Refac
        else:
            Node.ActiveTime -=1
    return

def Act(Node):
    if (Node.state=="$"):
        for x in range(len(Node.Neighbors)):
            if (Node.Neighbors[x].state=="."):
                Node.Neighbors[x].Current+= 1
    return

def FindNeighbors(Node,array):
    Neighdist= 3
    for a in range(len(array)):
        for b in range(len(array[a])):
            x=Nodes[a][b].posx-Node.posx
            y=Nodes[a][b].posy-Node.posy
            dist=x*x+y*y
            if (dist<Neighdist and dist!=0):
                Node.Neighbors.append(array[a][b])
    return


NxN=(50,50)
Nodes = [[Node(a,b) for a in range(NxN[0])]for b in range(NxN[1])]
for l in range(len(Nodes)):
    for m in range(len(Nodes)):
        FindNeighbors(Nodes[l][m],Nodes)

print(len(Nodes))



def set():
    TR = 6
    for a in range(len(Nodes)):
        for b in range(len(Nodes[a])):
            Nodes[a][b].state="."
            Nodes[a][b].Current = 0
            Nodes[a][b].RefactoryTime = 0
            Nodes[a][b].ActiveTime = 0
    # circular wave
    #    Nodes[25][25].Current = TR
    #   Nodes[24][25].Current = TR
    #   Nodes[24][24].Current = TR
    #   Nodes[25][24].Current = TR

    # One Spiral Wave
    for a in range(25):
        Nodes[a][24].state = "*"
        Nodes[a][24].RefactoryTime = 6
    for b in range(25):
        Nodes[b][25].Current = TR
    # another spiral wave
    #    for a in range(10):
    #        Nodes[a][40].state = "*"
    #        Nodes[a][40].RefactoryTime=6
    #    for b in range(10):
    #        Nodes[b][41].Current =TR
    #        Nodes[25][a].state = "*"
    #        Nodes[25][a].RefactoryTime=19


def cenmass(array):
    count = 0.0
    xcm = 0.0
    ycm = 0.0
    for a in range(len(array)):
        for b in range(len(array[a])):
            if (Nodes[a][b].state == "$"):
                xcm += a
                ycm += b
                count += 1
    cenx = (xcm / count)
    ceny = (ycm / count)
    print(cenx, ceny)
    for i in range(30):
        acount = 0
        for a in range(len(array)):
            for b in range(len(array[a])):
                x = Nodes[a][b].posx - cenx
                y = Nodes[a][b].posy - ceny
                dist = sqrt(x * x + y * y)
                if (dist <= i and Nodes[a][b].state == "$"):
                    acount += 1
        func[i] = acount

    for i in range(29):
        func2[i] = func[i+1] - func[i]











#Nodes=np.zeros((30,30),dtype=int)
np.set_printoptions(threshold=np.nan)
steps=1

class Interface(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master)
        self.Createwig()

    def Createwig(self):
        self.tex = tk.Text(width=150, height=55)
        self.tex.pack(side="left")
        self.butt = tk.Button(width=20, height=3)
        self.butt["text"] = "Go"
        self.buttset = tk.Button(width=20, height=3)
        self.butt["command"] = self.random
        self.buttset["text"] = "set"
        self.buttset["command"] = set
        self.calbutt = tk.Button(width=20, height=3, command=self.centremass)
        self.calbutt["text"] = "calculate "
        self.upbutt = tk.Button(width=20, height=3, command=self.updategraph)
        self.upbutt["text"] = "Graph"
        self.butt.pack()
        self.buttset.pack()
        self.calbutt.pack()
        self.upbutt.pack()

    def random(self):

        num=int(rm.randrange(0,9))


        for x in range(steps):

            self.tex.delete(0.0, 10001.0)
            for l in range(len(Nodes)):
                self.tex.insert('end', "\n")
                for m in range(len(Nodes)):
                    self.tex.insert('end', str(Nodes[l][m].state) + "  ")
            for l in range(len(Nodes)):
                for m in range(len(Nodes)):
                    ReAct(Nodes[l][m])
            for l in range(len(Nodes)):
                for m in range(len(Nodes)):
                    Act(Nodes[l][m])

    def centremass(self):
        cenmass(Nodes)

    def updategraph(self):
        x = np.linspace(0, 29, 30)

        fig = Figure(figsize=(6, 6))
        a = fig.add_subplot(111)
        a.plot(x, [func2[i] for i in range(30)], label='cubic')

        a.set_title("Estimation Grid", fontsize=16)
        a.set_ylabel("Y", fontsize=14)
        a.set_xlabel("X", fontsize=14)

        canvas = FigureCanvasTkAgg(fig, master=root)
        canvas.get_tk_widget().pack()
        canvas.draw()

    def end(self):
        self.tex.delete(0.0, 10.0)
        self.tex.insert('end', "thank you")

        #self.delay()
        root.destroy()
    def delay(self):
        clock.sleep(1)



root = tk.Tk()
screen = Interface(master=root)

screen.mainloop()