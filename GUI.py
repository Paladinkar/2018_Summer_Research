import tkinter as tk
from tkinter import ttk
import time as clock
import random as rm
import numpy as np
from PIL import Image, ImageTk
from math import *
ran = rm.random
x=0
y=0
Refac=20
Activ=2

class Node:
    def __init__(self,x,y):
        self.Current = 0
        self.RefactoryTime=0
        self.ActiveTime=0
        self.Neighbors=[]
        self.state = "."
        self.posx =x
        self.posy = y

def ReAct(Node): #Change of state and cool-down periods
    if (Node.state=="*"):
        if (Node.RefactoryTime==0):
            Node.state="."
            Node.Current=0
        else:
            Node.RefactoryTime -=1



    else:
        if (Node.state=="."):
            if (Node.Current>rm.randint(1,1)):
                Node.state="$"
                Node.ActiveTime=rm.randint(2,5)
                Node.Current-=1
            else:
                if (Node.Current>0):
                    Node.Current -=1



        else:
            if (Node.state=="$"):
                if(Node.ActiveTime==0):
                    Node.state="*"
                    Node.RefactoryTime=rm.randint(3,9)
                else:
                    Node.ActiveTime -=1
    return
def Act(Node):
    if (Node.state=="$"):
        for x in range(len(Node.Neighbors)):
            if (Node.Neighbors[x].state=="."):
                Node.Neighbors[x].Current+= 1
    return

def FindNeighbors(Node,array):
    x=Node.posx
    y=Node.posy
    #x
    if (Node.posx>0 ):
        Node.Neighbors.append(array[x-2][y])
    if (Node.posx < len(array)):
        Node.Neighbors.append(array[x][ y])
    #y
    if (Node.posx > 0):
        Node.Neighbors.append(array[x][y-2])
    if (Node.posx < len(array[0])):
        Node.Neighbors.append(array[x][y])

    return

NxN=(100,100)
Nodes = [[Node(a,b) for a in range(NxN[0])]for b in range(NxN[1])]
for l in range(len(Nodes)):
    for m in range(len(Nodes)):
        FindNeighbors(Nodes[l][m],Nodes)

print(len(Nodes))


def set():
    TR = 6
    for a in range(len(Nodes)):
        for b in range(len(Nodes[a])):
            Nodes[a][b].state="."
            Nodes[a][b].Current = 0
            Nodes[a][b].RefactoryTime = 0
            Nodes[a][b].ActiveTime = 0
    Nodes[10][24].Current=TR
    Nodes[9][23].Current=TR
    Nodes[10][23].Current=TR
    Nodes[30][24].Current=TR
    Nodes[30][23].Current=TR
    Nodes[30][23].Current=TR
    for a in range(len(Nodes)):
        Nodes[a][25].state = "*"
        Nodes[a][25].RefactoryTime=19
        Nodes[25][a].state = "*"
        Nodes[25][a].RefactoryTime=19


#Nodes=np.zeros((30,30),dtype=int)
np.set_printoptions(threshold=np.nan)
steps=400

class Interface(tk.Frame):
    def __init__(self,master=None):
        super().__init__(master)
        self.Createwig()
    def Createwig(self):
        self.tex = tk.Text(width=150, height=55)
        self.tex.pack(side="left")
        self.butt= tk.Button(width=20,height=3)
        self.butt["text"]="Go"
        self.buttset = tk.Button(width=20, height=3)
        self.butt["command"]= self.random
        self.buttset["text"]="set"
        self.buttset["command"]= set
        self.butt.pack()
        self.buttset.pack()
        self.exit = tk.Button(width=20,height=3,text='Quit', fg="red",command=self.end)
        self.exit.pack(side="right")




    def random(self):

        num=int(rm.randrange(0,9))


        for x in range(steps):


            for l in range(len(Nodes)):
                for m in range(len(Nodes)):
                    ReAct(Nodes[l][m])
            for l in range(len(Nodes)):
                for m in range(len(Nodes)):
                    Act(Nodes[l][m])
            self.tex.delete(0.0, 'end')
            for l in range(len(Nodes)):
                self.tex.insert('end', "\n")
                for m in range(len(Nodes)):
                    self.tex.insert('end', str(Nodes[l][m].state) + "  ")
            #self.tex.insert('end',rm.random())
            self.tex.update()

    def end(self):
        self.tex.delete(0.0, 10.0)
        self.tex.insert('end', "thank you")
        #self.delay()
        root.destroy()
    def delay(self):
        clock.sleep(1)



root = tk.Tk()
screen = Interface(master=root)

screen.mainloop()