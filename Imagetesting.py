import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
from matplotlib import animation
#import tkinter as tk
#from tkinter import ttk
import time as clock
import random as rm
import numpy as np
from PIL import Image, ImageTk
from math import *
import multiprocessing as mp

#Excel file tools
import openpyxl as pyxl
import openpyxl.chart
import openpyxl.chart.error_bar
import random
from openpyxl import Workbook, load_workbook
from openpyxl.chart import ScatterChart, Reference, Series
from openpyxl.chart.series_factory import SeriesFactory
from openpyxl.chart.error_bar import ErrorBars
from openpyxl.chart.data_source import NumDataSource, NumData, NumVal
#This def is for creating error bars, openpyxl is hell when it comes to this
def list2errorbars(plus, minus, errDir='y', errValType='cust'):
    "Returns ErrorBar from lists of error values"

    #Convert to list of NumVal
    numvals_plus = [NumVal(i, None, v=x) for i,x in enumerate(plus)]
    numvals_minus = [NumVal(i, None, v=x) for i,x in enumerate(minus)]

    # Convert to NumData
    nd_plus = NumData(pt=numvals_plus)
    nd_minus = NumData(pt=numvals_minus)

    # Convert to NumDataSource
    nds_plus = NumDataSource(numLit=nd_plus)
    nds_minus = NumDataSource(numLit=nd_minus)

    return ErrorBars(plus=nds_plus, minus=nds_minus, errDir=errDir, errValType=errValType)


#rm.seed(179874)
lower_A=1
upper_A=lower_A
lower_R=2
upper_R=lower_R

class Node:
    def __init__(self,x,y):
        self.Current = 0
        self.RefactoryTime=0
        self.ActiveTime=0
        self.Neighbors=[]
        self.state = "."
        self.posx =x
        self.posy = y
        #these following values are used for edge detection
        self.edgecontacts=[]
        self.fistring=[]
        self.secondring=[]
        self.thirdring=[]
        self.edgecase=0
        self.centercase=0
def ReAct(Node): #Change of state and cool-down periods
    if (Node.state=="*"):
        if (Node.RefactoryTime==0):
            Node.state="."
            Node.Current=0
        else:
            Node.RefactoryTime -=1



    else:
        if (Node.state=="."):
            if (Node.Current>1):
                Node.state="$"
                Node.ActiveTime=rm.randint(lower_A,upper_A)
            #Node.ActiveTime=Activ
                Node.Current=0
            else:
                if (Node.Current>0):
                    Node.Current -=1



        else:
            if (Node.state=="$"):
                if(Node.ActiveTime==0):
                    Node.state="*"
                    Node.RefactoryTime=rm.randint(lower_R,upper_R)
                    #Node.RefactoryTime=Refac
                else:
                    Node.ActiveTime -=1
    return
def Act(Node):
    if (Node.state=="$"):
        for x in range(len(Node.Neighbors)):
            if (Node.Neighbors[x].state=="."):
                Node.Neighbors[x].Current+= 1
    return

def FindNeighbors(Node,array):
    Neighdistreal=1.5
    Neighdist=ceil(Neighdistreal)
    Neighdistsqr=Neighdistreal*Neighdistreal
    nx=Node.posx
    ny=Node.posy
    xplus=Neighdist
    xminus=Neighdist
    yplus=Neighdist
    yminus=Neighdist
    ystart =ny-Neighdist
    xstart=nx-Neighdist
    #print("bong")
    if nx<Neighdist:
        xminus=nx
        xstart=0
    if nx>(len(array)-Neighdist):
        xplus=len(array)-nx
    if ny<Neighdist:
        yminus=ny
        ystart=0
    if ny>(len(array[0])-Neighdist):
        yplus=len(array[0])-ny

    for a in range(xplus+xminus):
        for b in range(yplus+yminus):
            x=Nodes[a+xstart][b+ystart].posx-nx
            y=Nodes[a+xstart][b+ystart].posy-ny

            dist=x*x+y*y
            if (dist<=(Neighdistsqr) and dist!=0):
                Node.Neighbors.append(Nodes[a+xstart][b+ystart])
            #this next bit is for edge detection
            if (dist<=(1) and dist!=0):
                Node.edgecontacts.append(Nodes[a+xstart][b+ystart])
            #print(str(xstart) + "|" + str(ystart))
            #print(str(nx)+"/"+str(ny))
    return
def Findrings(Node,Nodes):
    x=Node.posx-1
    y=Node.posy-1
    #start here, the we go around
    Node.fistring.append(Nodes[x][y])
    for a in range(2):
        x+=1
        Node.fistring.append(Nodes[x][y])
    for a in range(2):
        y += 1
        Node.fistring.append(Nodes[x][y])
    for a in range(2):
        x -= 1
        Node.fistring.append(Nodes[x][y])
    for a in range(1):
        y -= 1
        Node.fistring.append(Nodes[x][y])
    #second ring
    x=Node.posx-2
    y=Node.posy-2
    Node.secondring.append(Nodes[x][y])
    for a in range(4):
        x += 1
        Node.secondring.append(Nodes[x][y])
    for a in range(4):
        y += 1
        Node.secondring.append(Nodes[x][y])
    for a in range(4):
        x -= 1
        Node.secondring.append(Nodes[x][y])
    for a in range(3):
        y -= 1
        Node.secondring.append(Nodes[x][y])
    #third ring
    x=Node.posx-3
    y=Node.posy-3
    Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        x += 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        y += 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        x -= 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(5):
        y -= 1
        Node.thirdring.append(Nodes[x][y])


    return
#this is reponsible for the tracing of edges
def FindEdge(Nodes):
    checr=0
    for a in range(len(Nodes)):
        for b in range(len(Nodes)):
            Nodes[a][b].edgecase = 0
            if Nodes[a][b].state=="$":
                for c in range(len(Nodes[a][b].edgecontacts)):
                    if Nodes[a][b].edgecontacts[c].state==".":
                        checr+=1
                if checr >0:
                    Nodes[a][b].edgecase=1
                checr=0

    return
InitalCenter=[]
InitalCenterlast=[24,23]
Centbuffer=0
Wavecount=[]
BuffferRecrd=[]
timeout=0
def Findcenter(Nodes):
    global timeout
    global Centbuffer
    global InitalCenterlast
    trackedcenter=0
    suspectedCenter=[0,0]
    count=0
    one=0
    two=0
    record=[]
    record2 = []
    three=0
    for a in range(3,len(Nodes)-3):
        for b in range(3,len(Nodes)-3):
            Nodes[a][b].centercase = 0
            if Nodes[a][b].edgecase==1:
                for c in range(len(Nodes[a][b].fistring)):

                    if Nodes[a][b].fistring[c].edgecase==1:
                        #print(Nodes[a][b].fistring[c].edgecase)
                        record.append(Nodes[a][b].fistring[c])
                        one += 1
                for c in range(len(Nodes[a][b].secondring)):
                    if Nodes[a][b].secondring[c].edgecase==1:
                        #print(Nodes+[a][b].fistring[c].edgecase)
                        record2.append(Nodes[a][b].secondring[c])
                        two += 1
                """for c in range(len(Nodes[a][b].thirdring)):
                    if Nodes[a][b].thirdring[c].edgecase==1:
                        #print(Nodes+[a][b].fistring[c].edgecase)
                        three += 1"""
                #if len(record)>2:print(len(record))
                if one <=2 and one>0 and two<=2 :

                    if one==2:
                        x=record[0].posx-record[1].posx
                        y=record[0].posy-record[1].posy
                        xsqr=x*x
                        ysqr=y*y
                        magsqr=ysqr+xsqr


                        if magsqr<=1:
                            if two == 2:
                                x = record2[0].posx - record2[1].posx
                                y = record2[0].posy - record2[1].posy
                                xsqr = x * x
                                ysqr = y * y
                                magsqr = ysqr + xsqr
                                if magsqr <= 2:
                                    Nodes[a][b].centercase = 1
                                    count += 1
                            else:
                                Nodes[a][b].centercase = 1
                                count += 1
                    else:
                        if two == 2:
                            x = record2[0].posx - record2[1].posx
                            y = record2[0].posy - record2[1].posy
                            xsqr = x * x
                            ysqr = y * y
                            magsqr = ysqr + xsqr

                            if magsqr <= 3:
                                Nodes[a][b].centercase = 1
                                count+=1
                        else:
                            Nodes[a][b].centercase = 1
                            count += 1
                if timeout==0 and Nodes[a][b].centercase==1:
                    xdis=InitalCenterlast[0]-Nodes[a][b].posx
                    ydis=InitalCenterlast[1]-Nodes[a][b].posy
                    magsqr=xdis*xdis+ydis*ydis
                    if magsqr<=(Centbuffer+1):
                        trackedcenter+=1
                        suspectedCenter=[Nodes[a][b].posx,Nodes[a][b].posy]


            record2 = []
            record=[]
            one=0
            two=0
            three=0
    if timeout==0 and trackedcenter==1:
        if Centbuffer>0:
            x=(suspectedCenter[0]-InitalCenterlast[0])/Centbuffer
            y=(suspectedCenter[1]-InitalCenterlast[1])/Centbuffer
            for a in range(Centbuffer):
                InitalCenterlast[0]=int(round(suspectedCenter[0]+x))
                InitalCenterlast[1]=int(round(suspectedCenter[1]+y))
                InitalCenter.append(InitalCenterlast)
            InitalCenterlast = suspectedCenter
        else:
            InitalCenterlast = suspectedCenter
            InitalCenter.append(suspectedCenter)
        BuffferRecrd.append(0)
        Centbuffer=0
    else:
        BuffferRecrd.append(1)
        Centbuffer+=1
        if Centbuffer>10:
            timeout=1

    return count
NxN=50
#a&b are revesed just because it works this way
Nodes = [[Node(b,a) for a in range(NxN)]for b in range(NxN)]
#Array for highlighting
Traces = [[0 for a in range(NxN-6)]for b in range(NxN-6)]
#calling def to find neighbors
for l in range(len(Nodes)):
    for m in range(len(Nodes)):
        FindNeighbors(Nodes[l][m],Nodes)
#used for our edge detection, a box slightly smaller that the original array
for l in range(3,len(Nodes)-3):
    for m in range(3,len(Nodes)-3):
        Findrings(Nodes[l][m],Nodes)


#Node for timing of frequencies
Listeningpost=Nodes[0][0]
Listeningpost.time=0
Listeningpost.frequecies=[]
Listeningpost.run=False
Listeningpost.priorstate="."


periods=[]
def Listen():
    val=0
    if Listeningpost.run==True:
        if (Listeningpost.state=="$" and Listeningpost.priorstate=="."):
            Listeningpost.frequecies.append(Listeningpost.time)
            periods.append(Listeningpost.time)
            Listeningpost.time=0
        Listeningpost.time+=1
    else:
        if (Listeningpost.state == "$" and Listeningpost.priorstate == "."):
            Listeningpost.run=True
    Listeningpost.priorstate=Listeningpost.state
    #print(Listeningpost.frequecies)
def set(i,l):
    TR = 6
    global upper_A
    global upper_R
    global Listeningpost
    global InitalCenter
    global Wavecount
    global avgspirals
    global  Centbuffer
    Centbuffer=0
    Listeningpost.frequecies=[]
    InitalCenter=[]
    Wavecount=[]
    avgspirals = 0
    global InitalCenterlast
    InitalCenterlast = [24, 23]
    global timeout
    timeout=0
    upper_A=i
    upper_R=l
    for a in range(len(Nodes)):
        for b in range(len(Nodes[a])):
            Nodes[a][b].state="."
            Nodes[a][b].Current = 0
            Nodes[a][b].RefactoryTime = 0
            Nodes[a][b].ActiveTime = 0
    """Nodes[10][24].Current=TR
    Nodes[9][23].Current=TR
    Nodes[10][23].Current=TR"""

    for a in range(24):
        Nodes[a][24].Current=TR
    for a in range(len(Nodes)):
        Nodes[a][25].state = "*"
        Nodes[a][25].RefactoryTime=4


set(2,5)
#Nodes=np.zeros((30,30),dtype=int)
#np.set_printoptions(threshold=np.nan)

steps=1
periods=[]

fig = plt.figure()
data = np.zeros((NxN, NxN))
im = plt.imshow(data, cmap='gist_ncar', vmin=0, vmax=100)

avgspirals = 0
### NOTICE: temp is responsible for color, x & y are swaped on the graph
def sim(i):
    temp = np.zeros([NxN,NxN],dtype=int)
    global avgspirals
    for x in range(steps):

        for l in range(len(Nodes)):
            for m in range(len(Nodes)):
                ReAct(Nodes[l][m])

                temp[l, m] = 0
        FindEdge(Nodes)
        avgspirals+=Findcenter(Nodes)
        if i%10==0:
            Wavecount.append(avgspirals/10)
            #print(avgspirals / 10)
            avgspirals=0
        for l in range(len(Nodes)):
            for m in range(len(Nodes)):
                Act(Nodes[l][m])


                if(Nodes[l][m].state=="$"):
                    temp[l,m]=12
                else:
                    if(Nodes[l][m].state=="*"): temp[l,m]= 6

                if(Nodes[l][m].edgecase==1):
                    temp[l,m]=20
                    if(Nodes[l][m].centercase==1):
                        temp[l,m]=70
                temp[InitalCenterlast[0],InitalCenterlast[1]]=100

        #print(InitalCenter)
        Listen()

        im.set_data(temp)

#just getting velocities & average velocites


print("Starting sims")

UpperA=2
UpperR=5
SimSteps=1000
SimPages=200
InitalCenterCount=[0 for a in range(SimSteps)]

filedisonly=Workbook()
#Decicate excel for distance
dissheet=filedisonly.create_sheet("Y dis")
dissheet=filedisonly.create_sheet("Mag dis")
alp=filedisonly.sheetnames[0]
dissheet=filedisonly[alp]
dissheet.title="X dis"

file=Workbook()
for i in range(SimPages):
    rm.seed(i*64)
    set(UpperA,UpperR)
    for a in range(SimSteps):
        sim(a)
    print("Run [A:"+str(lower_A)+"-"+str(upper_A)+"R:"+str(lower_R)+"-"+str(upper_R)+"} is "+str((i / SimPages)*100) + "% done")
    sheet = file.create_sheet("Run#"+str(i+1))
    sheet.cell(row=1,column=1,value="pos x")
    for a in range(len(InitalCenter)):
        sheet.cell(row=a+2,column=1,value=InitalCenter[a][0])
    sheet.cell(row=1,column=2,value="pos y")
    for a in range(len(InitalCenter)):
        sheet.cell(row=a+2,column=2,value=InitalCenter[a][1])
    sheet.cell(row=1,column=3,value="# waves")
    for a in range(len(Wavecount)):
        sheet.cell(row=a+2,column=3,value=Wavecount[a])
    sheet.cell(row=1,column=4,value="spd inst")
    avginstspeed = 0
    for a in range(2,len(InitalCenter)):
        x1=InitalCenter[a-1][0]
        y1=InitalCenter[a-1][1]
        x2=InitalCenter[a][0]
        y2=InitalCenter[a][1]
        dist=(x2-x1)*(x2-x1)+(y2-y2)*(y2-y1)
        dist=pow(dist,.5)
        avginstspeed+=dist
    if len(InitalCenter)>2:
        avginstspeed/=(len(InitalCenter)-1)
    else: avginstspeed=0
    sheet.cell(row=2, column=4, value=avginstspeed)

    sheet.cell(row=1,column=5,value="spd drift")
    avgdrifspeed = 0
    for a in range(2,len(InitalCenter)):
        if a%10==0:
            x1=InitalCenter[a-10][0]
            y1=InitalCenter[a-10][1]
            x2=InitalCenter[a][0]
            y2=InitalCenter[a][1]
            dist=(x2-x1)*(x2-x1)+(y2-y2)*(y2-y1)
            dist=pow(dist,.5)
            avgdrifspeed+=dist
    if len(InitalCenter) > 2:
        avgdrifspeed/=(len(InitalCenter)-1)
    else: avgdrifspeed=0
    sheet.cell(row=2, column=5, value=avgdrifspeed)

    sheet.cell(row=1,column=6,value="Periods")
    for a in range(len(Listeningpost.frequecies)):
        sheet.cell(row=(a+2), column=6, value=Listeningpost.frequecies[a])

    sheet.cell(row=1,column=7,value="dN/dt")
    for a in range(len(Wavecount)-1):
        dN=Wavecount[a+1]-Wavecount[a]
        sheet.cell(row=(a+2),column=7,value=dN)
    velx=[(InitalCenter[a][0]-InitalCenter[a+1][0]) for a in range(len(InitalCenter)-1)]
    vely=[(InitalCenter[a][1]-InitalCenter[a+1][1]) for a in range(len(InitalCenter)-1)]
    disx=[(InitalCenter[a][0]-InitalCenter[2][0]) for a in range(2,len(InitalCenter)-1)]
    disy=[(InitalCenter[a][1]-InitalCenter[2][1]) for a in range(2,len(InitalCenter)-1)]
    magdis=[sqrt(disx[a]*disx[a]+disy[a]*disy[a]) for a in range(len(disx))]
    avgvelx=np.mean(velx)
    avgvely=np.mean(vely)
    avgvel_mag = sqrt(avgvelx**2+avgvely**2)
    sheet.cell(row=1, column=8, value="vel x")
    for a in range(len(velx)):
        sheet.cell(row=a + 2, column=8, value=velx[a])
    sheet.cell(row=1, column=9, value="vel y")
    for a in range(len(vely)):
        sheet.cell(row=a + 2, column=9, value=vely[a])
    sheet.cell(row=1, column=10, value="avgvelx")
    sheet.cell(row=2, column=10, value=avgvelx)
    sheet.cell(row=1, column=11, value="avgvely")
    sheet.cell(row=2, column=11, value=avgvely)
    sheet.cell(row=1, column=12, value="avgvely_mag")
    sheet.cell(row=2, column=12, value=avgvel_mag)
    sheet.cell(row=1,column=13,value="x dis")
    for a in range(len(disx)):
        sheet.cell(row=a + 2, column=13, value=disx[a])
        dissheet=filedisonly["X dis"]
        dissheet.cell(row=a+1, column=i+1,value=disx[a])
    sheet.cell(row=1, column=14, value="y dis")
    for a in range(len(disy)):
        sheet.cell(row=a + 2, column=14, value=disy[a])
        dissheet=filedisonly["Y dis"]
        dissheet.cell(row=a+1, column=i+1,value=disy[a])
    sheet.cell(row=1, column=15, value="mag dis")
    for a in range(len(disy)):
        sheet.cell(row=a + 2, column=15, value=magdis[a])
        dissheet=filedisonly["Mag dis"]
        dissheet.cell(row=a+1, column=i+1,value=magdis[a])
    displacement = 0
    sheet.cell(row=1,column=17,value="seed #")
    sheet.cell(row=2,column=17,value=i*64)
    for a in range(len(InitalCenter)):
        InitalCenterCount[a]+=1

AvgerageWavecount=[]
ErrWavecount=[]
averagespd_I = []
averagespd_D = []
average = []
DnDtAvg=[[]]
PeriodAvg=[[]]
XAvgDis = [[]]
YAvgDis = [[]]
MagAvgDis = [[]]
for c in range(len(file.sheetnames) - 1):
    Sname = file.sheetnames[c + 1]
    active = file[Sname]
    for v in range(SimSteps):
        if active.cell(row=v+2, column=7).value != None:
            if len(DnDtAvg)<=v:
                DnDtAvg.append([])
            DnDtAvg[v].append(active.cell(row=v + 2, column=7).value)
        else: break
    for v in range(SimSteps):
        if active.cell(row=v+2, column=6).value != None:
            if len(PeriodAvg)<=v:
                PeriodAvg.append([])
            PeriodAvg[v].append(active.cell(row=v + 2, column=6).value)
        else: break
    for v in range(SimSteps):
        if active.cell(row=v+2, column=13).value != None:
            if len(XAvgDis)<=v:
                XAvgDis.append([])
            XAvgDis[v].append(active.cell(row=v + 2, column=13).value)
        else: break
    for v in range(SimSteps):
        if active.cell(row=v+2, column=14).value != None:
            if len(YAvgDis)<=v:
                YAvgDis.append([])
            YAvgDis[v].append(active.cell(row=v + 2, column=14).value)
        else: break
    for v in range(SimSteps):
        if active.cell(row=v+2, column=15).value != None:
            if len(MagAvgDis)<=v:
                MagAvgDis.append([])
            MagAvgDis[v].append(active.cell(row=v + 2, column=15).value)
        else: break
    averagespd_I.append(active.cell(row=(2), column=4).value)
    averagespd_D.append(active.cell(row=(2), column=5).value)


AvgDnDtAvg=[np.mean(DnDtAvg[a]) for a in range(len(DnDtAvg))]
AvgDnDtAvgerror=[np.std(DnDtAvg[a]) for a in range(len(DnDtAvg))]
AvgXAvgdis=[np.mean(XAvgDis[a]) for a in range(len(XAvgDis))]
AvgXAvgdiserror=[np.std(XAvgDis[a]) for a in range(len(XAvgDis))]
AvgYAvgdis=[np.mean(YAvgDis[a]) for a in range(len(YAvgDis))]
AvgYAvgdiserror=[np.std(YAvgDis[a]) for a in range(len(YAvgDis))]
AvgMagAvgDis=[np.mean(MagAvgDis[a]) for a in range(len(MagAvgDis))]
AvgMagAvgDiserror=[np.std(MagAvgDis[a]) for a in range(len(MagAvgDis))]
AvgPeriodAvg=[np.mean(PeriodAvg[a]) for a in range(len(PeriodAvg))]
AvgPeriodAvgerror=[np.std(PeriodAvg[a]) for a in range(len(PeriodAvg))]

averagespd_I_Total=np.mean(averagespd_I)
averagespd_D_Total=np.mean(averagespd_D)
averagespd_I_err=np.std(averagespd_I)/sqrt(len(averagespd_I))
averagespd_D_err=np.std(averagespd_D)/sqrt(len(averagespd_D))
for a in range(len(Wavecount)):
    average=[]
    for c in range(len(file.sheetnames)-1):
        Sname=file.sheetnames[c+1]
        active=file[Sname]
        average.append(active.cell(row=(a+2),column=3).value)
        averagespd_I.append(active.cell(row=(a + 2), column=4).value)
        averagespd_I.append(active.cell(row=(a + 2), column=5).value)
    AvgerageWavecount.append(np.mean(average))
    ErrWavecount.append(np.std(average)/sqrt(len(average)))
    Sname = file.sheetnames[0]
    active = file[Sname]
    active.cell(row=a+2, column=1, value=AvgerageWavecount[a] )
    active.cell(row=a+2, column=2, value=ErrWavecount[a])
Sname = file.sheetnames[0]
active = file[Sname]
for a in range(len(InitalCenterCount)):
    active.cell(row=a+2, column=3, value=InitalCenterCount[a])
for a in range(len(AvgXAvgdis)):
    active.cell(row=a+2, column=8, value=AvgXAvgdis[a])
    active.cell(row=a + 2, column=9, value=AvgXAvgdiserror[a])
for a in range(len(AvgYAvgdis)):
    active.cell(row=a+2, column=10, value=AvgYAvgdis[a])
    active.cell(row=a + 2, column=11, value=AvgYAvgdiserror[a])
for a in range(len(AvgMagAvgDis)):
    active.cell(row=a+2, column=12, value=AvgMagAvgDis[a])
    active.cell(row=a + 2, column=13, value=AvgMagAvgDiserror[a])
for a in range(len(AvgPeriodAvg)):
    active.cell(row=a+2, column=14, value=AvgPeriodAvg[a])
    active.cell(row=a + 2, column=15, value=AvgPeriodAvgerror[a])
for a in range(len(AvgDnDtAvg)):
    active.cell(row=a+2, column=16, value=AvgDnDtAvg[a])
    active.cell(row=a + 2, column=17, value=AvgDnDtAvgerror[a])
results=file.sheetnames[0]
sheet=file[results]
sheet.title="results"
sheet.cell(row=1,column=1,value="Wavecount")
sheet.cell(row=1,column=2,value="Wavecount Error")
sheet.cell(row=1,column=2,value="# Initials tracked")
sheet.cell(row=1,column=4,value="Inst Spd")
sheet.cell(row=2,column=4,value=averagespd_I_Total)
sheet.cell(row=1,column=5,value="Inst err")
sheet.cell(row=2,column=5,value=averagespd_I_err)
sheet.cell(row=1,column=6,value="Drift Spd")
sheet.cell(row=2,column=6,value=averagespd_D_Total)
sheet.cell(row=1,column=7,value="Drift err")
sheet.cell(row=2,column=7,value=averagespd_D_err)
sheet.cell(row=1,column=8,value="X displacement")
sheet.cell(row=1,column=9,value="X displacement err")
sheet.cell(row=1,column=10,value="Y displacement")
sheet.cell(row=1,column=11,value="Y displacement err")
sheet.cell(row=1,column=12,value="Mag displacement")
sheet.cell(row=1,column=13,value="Mag displacement err")
sheet.cell(row=1,column=14,value="periods")
sheet.cell(row=1,column=15,value="periods err")
sheet.cell(row=1,column=16,value="Dn/dt")
sheet.cell(row=1,column=17,value="Dn/Dt err")
sheet.cell(row=1,column=17,value="Dn/Dt err")
#Graphing for easy reading
#wavcount graph
avgdata=pyxl.chart.Reference(sheet,min_col=1,min_row=2,max_col=1,max_row=len(AvgerageWavecount)+1)
WavecountChart=openpyxl.chart.LineChart()
WavecountChart.title = "Wavecount over time"
WavecountChart.style = 10
WavecountChart.y_axis.title = 'Number of Waves'
WavecountChart.x_axis.title = 'time per ten steps'
WavecountChart.add_data(avgdata,titles_from_data=False)
Cavg = WavecountChart.series[0]
Cavg.graphicalProperties.line.solidFill = "00AAAA"
Cavg.graphicalProperties.line.width = 30000

Cavgerr=openpyxl.chart.Series(avgdata,title="Error")
Cavg.errBars=list2errorbars(plus=ErrWavecount,minus=ErrWavecount,errValType='cust',errDir='y')
sheet.add_chart(WavecountChart,'A20')
#DnDt graph
avgDnDtdata=pyxl.chart.Reference(sheet,min_col=16,min_row=2,max_col=16,max_row=len(AvgDnDtAvg)+1)
DnDtcountChart=openpyxl.chart.LineChart()
DnDtcountChart.title = "Dn/Dt over time"
DnDtcountChart.style = 10
DnDtcountChart.y_axis.title = 'Dn/Dt'
DnDtcountChart.x_axis.title = 'time per ten steps'
DnDtcountChart.add_data(avgDnDtdata,titles_from_data=False)
Dnavg = DnDtcountChart.series[0]
Dnavg.graphicalProperties.line.solidFill = "00AAAA"
Dnavg.graphicalProperties.line.width = 30000

Dnavgerr=openpyxl.chart.Series(avgDnDtdata,title="Error")
Dnavg.errBars=list2errorbars(plus=AvgDnDtAvgerror,minus=AvgDnDtAvgerror,errValType='cust',errDir='y')
sheet.add_chart(DnDtcountChart,'J20')
#period graph: Notice, this one is strange
avgPerdata=pyxl.chart.Reference(sheet,min_col=14,min_row=2,max_col=14,max_row=len(AvgPeriodAvg)+1)
PeravgDisChart=openpyxl.chart.LineChart()
PeravgDisChart.title = "Steps between activation of listening post"
PeravgDisChart.style = 10
PeravgDisChart.y_axis.title = 'Steps between(Period)'
PeravgDisChart.x_axis.title = 'Activation'
PeravgDisChart.add_data(avgPerdata,titles_from_data=False)
Peravg = PeravgDisChart.series[0]
Peravg.graphicalProperties.line.solidFill = "00AAAA"
Peravg.graphicalProperties.line.width = 30000

Perdisavgerr=openpyxl.chart.Series(avgPerdata,title="Error")
Peravg.errBars=list2errorbars(plus=AvgPeriodAvgerror,minus=AvgPeriodAvgerror,errValType='cust',errDir='y')
sheet.add_chart(PeravgDisChart,'S20')
#Xdis chart"""
avgXdata=pyxl.chart.Reference(sheet,min_col=8,min_row=2,max_col=8,max_row=len(AvgXAvgdis)+1)
XavgDisChart=openpyxl.chart.LineChart()
XavgDisChart.title = "X displacement over time"
XavgDisChart.style = 10
XavgDisChart.y_axis.title = 'X displacement of Waves'
XavgDisChart.x_axis.title = 'step'
XavgDisChart.add_data(avgXdata,titles_from_data=False)
Xavg = XavgDisChart.series[0]
Xavg.graphicalProperties.line.solidFill = "00AAAA"
Xavg.graphicalProperties.line.width = 30000

Xdisavgerr=openpyxl.chart.Series(avgXdata,title="Error")
Xavg.errBars=list2errorbars(plus=AvgXAvgdiserror,minus=AvgXAvgdiserror,errValType='cust',errDir='y')
sheet.add_chart(XavgDisChart,'A35')
#Ydis chart
avgYdata=pyxl.chart.Reference(sheet,min_col=10,min_row=2,max_col=10,max_row=len(AvgYAvgdis)+1)
YavgDisChart=openpyxl.chart.LineChart()
YavgDisChart.title = "Y displacement over time"
YavgDisChart.style = 10
YavgDisChart.y_axis.title = 'Y displacement of Waves'
YavgDisChart.x_axis.title = 'step'
YavgDisChart.add_data(avgYdata,titles_from_data=False)
Yavg = YavgDisChart.series[0]
Yavg.graphicalProperties.line.solidFill = "00AAAA"
Yavg.graphicalProperties.line.width = 30000

Ydisavgerr=openpyxl.chart.Series(avgYdata,title="Error")
Yavg.errBars=list2errorbars(plus=AvgYAvgdiserror,minus=AvgYAvgdiserror,errValType='cust',errDir='y')
sheet.add_chart(YavgDisChart,'J35')
#Mag dis chart
avgMagdata=pyxl.chart.Reference(sheet,min_col=12,min_row=2,max_col=12,max_row=len(AvgMagAvgDis)+1)
MagavgDisChart=openpyxl.chart.LineChart()
MagavgDisChart.title = "Mag displacement over time"
MagavgDisChart.style = 10
MagavgDisChart.y_axis.title = 'Mag displacement of Waves'
MagavgDisChart.x_axis.title = 'step'
MagavgDisChart.add_data(avgMagdata,titles_from_data=False)
Magavg = MagavgDisChart.series[0]
Magavg.graphicalProperties.line.solidFill = "00AAAA"
Magavg.graphicalProperties.line.width = 30000

Magdisavgerr=openpyxl.chart.Series(avgMagdata,title="Error")
Magavg.errBars=list2errorbars(plus=AvgMagAvgDiserror,minus=AvgMagAvgDiserror,errValType='cust',errDir='y')
sheet.add_chart(MagavgDisChart,'S35')
#Inital wavecount
ICdata=pyxl.chart.Reference(sheet,min_col=3,min_row=2,max_col=3,max_row=len(InitalCenterCount)+1)
ICChart=openpyxl.chart.LineChart()
ICChart.title = "# of tracked waves left"
ICChart.style = 10
ICChart.y_axis.title = '# of waves'
ICChart.x_axis.title = 'step'
ICChart.add_data(ICdata,titles_from_data=False)
ICavg = ICChart.series[0]
ICavg.graphicalProperties.line.solidFill = "00AAAA"
ICavg.graphicalProperties.line.width = 30000

sheet.add_chart(ICChart,'A55')

file.save("Runs/A_"+str(lower_A)+"-"+str(upper_A)+","+"R_"+str(lower_R)+"-"+str(upper_R)+".xlsx")
filedisonly.save("Runs/A_"+str(lower_A)+"-"+str(upper_A)+","+"R_"+str(lower_R)+"-"+str(upper_R)+"Distance_Only.xlsx")
def init():
    im.set_data(np.zeros((NxN, NxN)))


set(UpperA,UpperR)
xper = [a for a in range(len(periods))]
#plt.plot(xper,periods)
anim = animation.FuncAnimation(fig, sim, init_func=init, frames=2000,
                               interval=30)

plt.show()
