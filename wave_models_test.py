import tkinter as tk
import time
import sys

#this is Karl testing git integration in pycharm on his desktop

class Node:
    def __init__(self, state, excited_period, refrac_period):
        self.state = state
        # default to refrac for all nodes
        self.excited_period = excited_period
        self.refrac_period = refrac_period
        self.time_excited = 0
        self.time_refrac = 0
        self.neighbors = []
        if state == 1:
            self.color = "yellow"
        elif state == 0:
            self.color = "green"
        else:
            self.color = "blue"

        self.x = self.y = -1

    def switch_state(self):
        """if self.state == 1:
            self.state = 0
            self.color = "green"

            self.time_refrac = self.refrac_period
        else:
            self.state = 1
            self.color = "yellow"
            self.time_excited = self.excited_period
         """

        """0 is ground
        2 is excited
        1 is refrac"""

        if self.state == 2:
            self.state = 1
            self.color = "blue"
            self.time_refrac = self.refrac_period
            self.time_excited = 0

        elif self.state == 1:
            self.state = 0
            self.color = "green"
            self.time_excited = 0
            self.time_refrac = 0

        else:
            self.state = 2
            self.color = "yellow"
            self.time_refrac = 0
            self.time_excited = self.excited_period

    def update(self):
        """if self.state == 0:
            if self.time_refrac == 0:
                self.color = "green"
            else:
                self.time_refrac -= 1
            if self.time_refrac != 0:
                self.time_refrac -= 1
        else:
            if self.time_excited == 0:
                self.switch_state()
                return 1
            else:
                self.time_excited -= 1
        return 0
        """

        if self.state == 0:
            return 0
        elif self.state == 2:
            if self.time_excited == 0:
                self.switch_state()
                return 1
            else:
                self.time_excited -= 1
                return 0
        else:
            if self.time_refrac == 0:
                self.switch_state()
                return 1
            else:
                self.time_refrac -= 1
                return 0

    def set_excited_period(self, period):
        self.excited_period = period

    def set_refrac_period(self, period):
        self.refrac_period = period

class WaveModeler:

    def __init__(self, n, excited, refrac, canvas1, time_per_step, steps_per_update):
        self.update_list = []
        self.changed_state = []
        self.size = n

        ##GET RID OF THIS LATER
        self.width = self.height = 1000

        self.excited_period = excited
        self.refrac_period = refrac
        self.GUI = canvas1
        self.node_list = [[Node(0, self.excited_period, self.refrac_period) for y in range(n)] for x in range(n)]
        self.drawArray()
        for i in range(self.size):
            for j in range(self.size):
                self.get_neighbors(self.node_list[i][j])

        self.running = False

        #self.time_per_step = 100 #delay between steps (ms)
        #self.steps_per_update = 1 #steps per GUI redraw
        self.time_per_step = time_per_step
        self.steps_per_update = steps_per_update
        self.current_step = 0

        #usde to check nodes, can definitely delete later
        #self.printout = []

        #ask for width, height as part of startup window
        #maybe add a random node picker for excited nodes somewhere in array later

        """        #set up GUI
        self.controls = canvas2
        self.canvas2. """


    def flip_flag(self):
        self.running = not self.running

    def get_neighbors(self, node):
        #left
        if node.x != 0:
            node.neighbors.append(self.node_list[node.x - 1][node.y])
            # for diagonals
            if node.y != 0:
                node.neighbors.append(self.node_list[node.x - 1][node.y-1])
            if node.y != self.size - 1:
                node.neighbors.append(self.node_list[node.x - 1][node.y+1])

        #right
        if node.x != self.size - 1:
            node.neighbors.append(self.node_list[node.x + 1][node.y])

            # for diagonals
            if node.y != 0:
                node.neighbors.append(self.node_list[node.x + 1][node.y - 1])
            if node.y != self.size - 1:
                node.neighbors.append(self.node_list[node.x + 1][node.y + 1])

        #top and bottom
        if node.y != 0:
            node.neighbors.append(self.node_list[node.x][node.y-1])
        if node.y != self.size - 1:
            node.neighbors.append(self.node_list[node.x][node.y+1])


    def check_neighbors(self, y,x):
        """current = self.node_list[x][y]
        ##if current.state == 1 or (current.state == 0 and current.time_refrac > 0):
        #    if(current.update() == 1):
        #        self.changed_state.append(current)
        #    return
        if current.state == 1 or current.state == 2:
            if(current.update() == 1):
                self.changed_state.append(current)
            return

        counter = 0

        while(True):
            if x != 0:
                counter += self.node_list[x-1][y].state //2
                #for diagonals
                if y!=0:
                    counter += self.node_list[x-1][y-1].state //2
                if y!= self.size-1:
                    counter += self.node_list[x-1][y+1].state //2
            if counter >= 2:
                break

            if x != self.size-1:
                    counter += self.node_list[x+1][y].state //2

                    # for diagonals
                    if y != 0:
                        counter += self.node_list[x + 1][y - 1].state //2
                    if y != self.size - 1:
                        counter += self.node_list[x + 1][y + 1].state //2

            if counter >= 2:
                break
            elif counter == 0:
                if(current.update() == 1):
                    self.changed_state.append(current)
                return

            if y != 0:
                    counter += self.node_list[x][y - 1].state //2
            if y != self.size - 1:
                    counter += self.node_list[x][y + 1].state //2
            break

        if counter >= 2:
            #self.printout.append('x:' + str(x) + " y:" + str(y))           #used to see which nodes were updated
            self.update_list.append(current)
        else:
            if(current.update() == 1):
                self.changed_state.append(current)

        """
        current = self.node_list[x][y]

        if current.state == 1 or current.state == 2:
            if (current.update() == 1):
                self.changed_state.append(current)
            return

        counter = 0

        for neighbor in current.neighbors:
            counter += (neighbor.state //2)

        if counter >= 2:
            self.update_list.append(current)
        else:
            if (current.update() == 1):
                self.changed_state.append(current)

    def reset(self):
        self.running = False
        self.update_list = []
        self.changed_state = []

        self.node_list = [[Node(0, self.excited_period, self.refrac_period) for y in range(self.size)] for x in range(self.size)]
        self.drawArray()
        for i in range(self.size):
            for j in range(self.size):
                self.get_neighbors(self.node_list[i][j])

    def update_values(self, excited_ent, refrac_ent, steps_per_ent, time_per_ent):
        #self.running = False
        self.excited_period = int(excited_ent)
        self.refrac_period = int(refrac_ent)
        self.steps_per_update = int(steps_per_ent)
        self.time_per_step = int(time_per_ent)
        self.current_step = 0
        #self.running = True

    def mainloop(self):
        #print('starting loop')
        #print('checking nodes:')

        #while(play button active) ##want to only run while play button is activated, and want to stop play button on pause or stop
        #print(self.running)

        if(self.running):
            self.updateEverything()
        self.GUI.after(self.time_per_step, self.mainloop)

    def timing(f):
        def wrap(*args):
            time1 = time.time()
            ret = f(*args)
            time2 = time.time()
            print('%s function took %0.3f ms' % (f.__name__, (time2 - time1) * 1000.0))
            return ret
        return wrap

    @timing
    def updateEverything(self):
        #print('checking all nodes')
        for i in range(self.size):
            for j in range(self.size):
                # printstr = 'checking:' + str(i) + ',' + str(j)
                # print(printstr)
                self.check_neighbors(i, j)

        # print("number of nodes to update: " + str(len(self.update_list)))
        # print(self.printout)  ##need to uncomment printout in __init__

        # updating nodes from neighbors
        for node in self.update_list:
            node.switch_state()

        self.current_step += 1
        if self.current_step == self.steps_per_update:
            # update GUI
            # print("updating GUI")
            self.updateGUI()
            self.current_step = 0

        # clear update_list
        self.update_list.clear()

        #print("DONE")

        self.changed_state.clear()


    def drawArray(self):
        rows = cols = self.size

        ###GET RID OF THIS LATER
        width = height = 1000

        rect_width, rect_height = width // rows, height // cols
        # where // is the floor division operator ie floor(width/rows) & floor(height/cols)

        for y in range(self.size):
            for x in range(self.size):
                x0, y0 = x * rect_width, y * rect_height
                x1, y1, = x0 + rect_width - 1, y0 + rect_height - 1
                #node = drawn_nodes.create_rectangle(x0, y0, x1, y1, fill=self.node_list[x][y].color, width=0, tags=['A', 'CLICK_AREA'])
                node = self.GUI.create_rectangle(x0, y0, x1, y1, fill=self.node_list[x][y].color, width=0,
                                                    tags=['A', 'CLICK_AREA'])
                self.node_list[x][y].x = x
                self.node_list[x][y].y = y

        self.GUI.bind("<Button-1>", self.onClick)

        self.GUI.pack()

    def onClick(self, event):
        x = self.GUI.canvasx(event.x)
        y = self.GUI.canvasy(event.y)
        sq = self.GUI.find_closest(x, y)[0]
        coords = self.GUI.coords(sq)
        y = coords.pop()
        x = coords.pop()
        rows = cols = self.size

        ###GET RID OF THIS LATER
        width = height = 1000

        rect_width, rect_height = width // rows, height // cols

        x = x / rect_width
        y = y / rect_height
        #print(str(x) + ', ' + str(y))
        x = int(x)
        y = int(y)
        #print(str(x) + ', ' + str(y))


        self.node_list[x][y].switch_state()
        """
        temp_node = self.node_list[x][y]

           if temp_node.state == 1:
        temp_node.switch_state()

        if temp_node.state == 0 and temp_node.time_refrac != 0:
            temp_node.switch_state()

        else:
        temp_node.switch_state(0)
        """

        self.GUI.itemconfigure(sq, fill=self.node_list[x][y].color)

    @timing
    def updateGUI(self):
        """for y in range(self.size):
            for x in range(self.size):
                #print('trying to update color for: ' + str(200*x) + ',' + str(200*y))
                sq = self.GUI.find_closest((width/self.size)*x,(height/self.size)*y)[0]
                self.GUI.itemconfigure(sq, fill=self.node_list[x][y].color)
                self.GUI.pack() """

        ###GET RID OF THIS LATER
        width = height = 1000

        for cur_node in self.changed_state:
            sq = self.GUI.find_closest((width/self.size)*cur_node.x, (height/self.size)*cur_node.y)[0]
            self.GUI.itemconfigure(sq, fill=cur_node.color)
            #self.GUI.pack()
        for cur_node in self.update_list:
            sq = self.GUI.find_closest((width / self.size) * cur_node.x, (height / self.size) * cur_node.y)[0]
            self.GUI.itemconfigure(sq, fill=cur_node.color)
            #self.GUI.pack()


def main(n, excited, refrac, time_step, steps_per_update):

    """save a copy of the default values, these will be used in the case that I have
    no init conds (i.e. run from teh file) with an if statementm"""

    top = tk.Tk()
    top.title("wave_prop")
    node_grid = tk.Frame()
    controls = tk.Frame()
    frame_list = {node_grid, controls}
    for frame in frame_list:
        frame.pack()

    # perhaps adjust these based on either window size (dragging) or ask the user
    width, height = 1000, 1000

    # pass this into waveModeler
    drawn_nodes = tk.Canvas(node_grid, width=width, height=height)

    control_canvas = tk.Canvas(controls, width=200, height=200)
    play_button = tk.Button(control_canvas, text='Play/Pause', command=lambda: APP.flip_flag())
    play_button.grid(row=0, column=0)
    reset_button = tk.Button(control_canvas, text='Reset Nodes', command=lambda: APP.reset())
    reset_button.grid(row=0, column=1)

    step_per_redraw_label = tk.Label(control_canvas, text="Steps per redraw")
    step_per_redraw_label.grid(row=1, column=5)
    step_per_redraw_entry = tk.Entry(control_canvas, width=10)
    step_per_redraw_entry.grid(row=1, column=6)

    time_step_label = tk.Label(control_canvas, text='Time per step (ms)')
    time_step_label.grid(row=1, column=7)
    time_step_entry = tk.Entry(control_canvas, width=10)
    time_step_entry.grid(row=1, column=8)

    global_excite_period_label = tk.Label(control_canvas, text='Excited Period (steps)')
    global_excite_period_label.grid(row=0, column=5)
    global_exite_period_entry = tk.Entry(control_canvas, width=10)
    global_exite_period_entry.grid(row=0, column=6)

    global_refrac_period_label = tk.Label(control_canvas, text='Refractory Period (steps)')
    global_refrac_period_label.grid(row=0, column=7)
    global_refrac_period_entry = tk.Entry(control_canvas, width=10)
    global_refrac_period_entry.grid(row=0, column=8)

    update_button = tk.Button(control_canvas, text="Update values",
                              command=lambda: APP.update_values(global_exite_period_entry.get(),
                                                                global_refrac_period_entry.get(),
                                                                step_per_redraw_entry.get()
                                                                , time_step_entry.get()))
    update_button.grid(row=0, column=9)

    control_canvas.pack()

    APP = WaveModeler(int(n), int(excited), int(refrac), drawn_nodes, int(time_step), int(steps_per_update))
    global_refrac_period_entry.insert(0, APP.refrac_period)
    global_exite_period_entry.insert(0, APP.excited_period)
    time_step_entry.insert(0, APP.time_per_step)
    step_per_redraw_entry.insert(0, APP.steps_per_update)
    APP.mainloop()

    top.mainloop()


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])

#TODO
#make a popup come before running that asks for init paramters (n, time_step, periods, grid size)
#want to add play, pause, stop buttons, RESET
#multithreading
#want to add text fields for possible global excited & refrac times per node
#want to add an import field for a txt doc setting init_parameters
