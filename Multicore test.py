import time
import numpy as np
import sys
import multiprocessing as mp
from multiprocessing import Pool
import random
from matplotlib import pyplot as plt
from matplotlib import animation
import tkinter as tk
from tkinter import ttk
import random as rm
from math import *
starttime=time.time()

class Node:
    def __init__(self,x,y):
        self.Current = 0
        self.RefactoryTime=0
        self.ActiveTime=0
        self.Neighbors=[]
        self.state = "."
        self.posx =x
        self.posy = y
Nodes=[]
NxN = 70
Nodes = [[Node(a, b) for a in range(NxN)] for b in range(NxN)]
def ReAct(Node): #Change of state and cool-down periods
    if (Node.state=="*"):
        if (Node.RefactoryTime==0):
            Node.state="."
            Node.Current=0
        else:
            Node.RefactoryTime -=1



    else:
        if (Node.state=="."):
            if (Node.Current>1):
                Node.state="$"
                Node.ActiveTime=rm.randint(3,5)
                #Node.ActiveTime=Activ
                Node.Current=0
            else:
                if (Node.Current>0):
                    Node.Current -=1



        else:
            if (Node.state=="$"):
                if(Node.ActiveTime==0):
                    Node.state="*"
                    Node.RefactoryTime=rm.randint(2,6)
                    #Node.RefactoryTime=Refac
                else:
                    Node.ActiveTime -=1
    return
def Act(Node):
    if (Node.state=="$"):
        for x in range(len(Node.Neighbors)):
            if (Node.Neighbors[x].state=="."):
                Node.Neighbors[x].Current+= 2
    return

def FindNeighbors(Node,array):
    Neighdist=5
    for a in range(len(array)):
        for b in range(len(array[a])):
            x=Nodes[a][b].posx-Node.posx
            y=Nodes[a][b].posy-Node.posy
            dist=x*x+y*y
            if (dist<Neighdist and dist!=0):
                Node.Neighbors.append(array[a][b])
                #print("Ding")
    return
def init():
    im.set_data(np.zeros((nx, ny)))

def animate(i):

    im.set_data(np.random.random((nx,ny))*5)
    return im


def Neighbor_middle(i):
    size = len(Nodes[i])
    for a in range(size):
            FindNeighbors(Nodes[i][a],Nodes)
            #print("Node"+str(i)+str(a))
    return
endtime=time.time()
finaltime=endtime-starttime
print(finaltime)
if __name__=='__main__':
    p = Pool(processes=mp.cpu_count())
    x = 0
    y = 0
    Refac = 20
    Activ = 10

    starttime = time.time()

    p.map(Neighbor_middle, (range(NxN)))
    print(str(len(Nodes[0][0].Neighbors)) + "#")
    endtime = time.time()
    finaltime = endtime - starttime

    print(len(Nodes))



    TR = 6
    for a in range(len(Nodes)):
        for b in range(len(Nodes[a])):
            Nodes[a][b].state = "."
            Nodes[a][b].Current = 0
            Nodes[a][b].RefactoryTime = 0
            Nodes[a][b].ActiveTime = 0
    """Nodes[10][24].Current=TR
    Nodes[9][23].Current=TR
    Nodes[10][23].Current=TR"""
    Nodes[30][24].Current = TR
    Nodes[30][23].Current = TR
    Nodes[30][23].Current = TR
    for a in range(len(Nodes)):
        Nodes[a][25].state = "*"
        Nodes[a][25].RefactoryTime = 19
        Nodes[25][a].state = "*"
        Nodes[25][a].RefactoryTime = 19


    # Nodes=np.zeros((30,30),dtype=int)
    np.set_printoptions(threshold=np.nan)

    #set()
    steps = 1
    nx = NxN
    ny = NxN

    fig = plt.figure()
    data = np.zeros((nx, ny))
    im = plt.imshow(data, cmap='gist_ncar', vmin=0, vmax=100)

    print("going")


    def random(i):
        temp = np.zeros([NxN, NxN], dtype=int)

        for x in range(steps):

            for l in range(len(Nodes)):
                for m in range(len(Nodes)):
                    ReAct(Nodes[l][m])
                    temp[l, m] = 0
            for l in range(len(Nodes)):
                for m in range(len(Nodes)):
                    Act(Nodes[l][m])
                    if (Nodes[l][m].ActiveTime != 0):
                        temp[l, m] = (Nodes[l][m].ActiveTime + 80)
                    else:
                        if (Nodes[l][m].RefactoryTime != 0): temp[l, m] = Nodes[l][m].RefactoryTime + 50

            im.set_data(temp)
    print(finaltime)

    anim = animation.FuncAnimation(fig, random, init_func=init, frames=nx * ny,

                                   interval=10)
    plt.show()


