import matplotlib
#matplotlib.use("Agg")
from matplotlib import pyplot as plt
from matplotlib import animation

import tkinter as tk
from tkinter import ttk
import time as clock
import random as rm
import numpy as np
from PIL import Image, ImageTk
from math import *
import multiprocessing as mp

#Excel file tools
import openpyxl as pyxl
import openpyxl.chart
import openpyxl.chart.error_bar
import random
from openpyxl import Workbook, load_workbook
from openpyxl.chart import ScatterChart, Reference, Series
from openpyxl.chart.series_factory import SeriesFactory
from openpyxl.chart.error_bar import ErrorBars
from openpyxl.chart.data_source import NumDataSource, NumData, NumVal

rm.seed(214)
lower_A=1
upper_A=lower_A
lower_R=1
upper_R=lower_R

class Node:
    def __init__(self,x,y):
        self.Current = 0
        self.RefactoryTime=0
        self.ActiveTime=0
        self.Neighbors=[]
        self.state = "."
        self.posx =x
        self.posy = y
        #these following values are used for edge detection
        self.edgecontacts=[]
        self.fistring=[]
        self.secondring=[]
        self.thirdring=[]
        self.edgecase=0
        self.centercase=0
def ReAct(Node): #Change of state and cool-down periods
    if (Node.state=="*"):
        if (Node.RefactoryTime==0):
            Node.state="."
            Node.Current=0
        else:
            Node.RefactoryTime -=1



    else:
        if (Node.state=="."):
            if (Node.Current>1):
                Node.state="$"
                Node.ActiveTime=rm.randint(lower_A,upper_A)
                #Node.ActiveTime=Activ
                Node.Current=0
            else:
                if (Node.Current>0):
                    Node.Current =0


        else:
            if (Node.state=="$"):
                if(Node.ActiveTime==0):
                    Node.state="*"
                    Node.RefactoryTime=rm.randint(lower_R,upper_R)
                    #Node.RefactoryTime=Refac
                else:
                    Node.ActiveTime -=1
    return
def Act(Node):
    if (Node.state=="$"):
        for x in range(len(Node.Neighbors)):
            if (Node.Neighbors[x].state=="."):
                Node.Neighbors[x].Current+= 1
    return

def FindNeighbors(Node,array):
    Neighdistreal=1.5
    Neighdist=ceil(Neighdistreal)
    Neighdistsqr=Neighdistreal*Neighdistreal
    nx=Node.posx
    ny=Node.posy
    xplus=Neighdist
    xminus=Neighdist
    yplus=Neighdist
    yminus=Neighdist
    ystart =ny-Neighdist
    xstart=nx-Neighdist

    if nx<Neighdist:
        xminus=nx
        xstart=0
    if nx>(len(array)-Neighdist):
        xplus=len(array)-nx
    if ny<Neighdist:
        yminus=ny
        ystart=0
    if ny>(len(array[0])-Neighdist):
        yplus=len(array[0])-ny

    for a in range(xplus+xminus):
        for b in range(yplus+yminus):
            x=Nodes[a+xstart][b+ystart].posx-nx
            y=Nodes[a+xstart][b+ystart].posy-ny

            dist=x*x+y*y
            if (dist<=(Neighdistsqr) and dist!=0):
                Node.Neighbors.append(Nodes[a+xstart][b+ystart])
            #this next bit is for edge detection
            if (dist<=(1) and dist!=0):
                Node.edgecontacts.append(Nodes[a+xstart][b+ystart])
            #print(str(xstart) + "|" + str(ystart))
            #print(str(nx)+"/"+str(ny))
    return
def Findrings(Node,Nodes):
    x=Node.posx-1
    y=Node.posy-1
    #start here, the we go around
    Node.fistring.append(Nodes[x][y])
    for a in range(2):
        x+=1
        Node.fistring.append(Nodes[x][y])
    for a in range(2):
        y += 1
        Node.fistring.append(Nodes[x][y])
    for a in range(2):
        x -= 1
        Node.fistring.append(Nodes[x][y])
    for a in range(1):
        y -= 1
        Node.fistring.append(Nodes[x][y])
    #second ring
    x=Node.posx-2
    y=Node.posy-2
    Node.secondring.append(Nodes[x][y])
    for a in range(4):
        x += 1
        Node.secondring.append(Nodes[x][y])
    for a in range(4):
        y += 1
        Node.secondring.append(Nodes[x][y])
    for a in range(4):
        x -= 1
        Node.secondring.append(Nodes[x][y])
    for a in range(3):
        y -= 1
        Node.secondring.append(Nodes[x][y])
    #third ring
    x=Node.posx-3
    y=Node.posy-3
    Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        x += 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        y += 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        x -= 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(5):
        y -= 1
        Node.thirdring.append(Nodes[x][y])


    return
#this is reponsible for the tracing of edges
def FindEdge(Nodes):
    checr=0
    for a in range(len(Nodes)):
        for b in range(len(Nodes)):
            Nodes[a][b].edgecase = 0
            if Nodes[a][b].state=="$":
                for c in range(len(Nodes[a][b].edgecontacts)):
                    if Nodes[a][b].edgecontacts[c].state==".":
                        checr+=1
                if checr >0:
                    Nodes[a][b].edgecase=1
                checr=0

    return
InitalCenter=[]
InitalCenterlast=[24,23]
Centbuffer=0
Wavecount=[]
BuffferRecrd=[]
timeout=0
def Findcenter(Nodes):
    global timeout
    global Centbuffer
    global InitalCenterlast
    trackedcenter=0
    suspectedCenter=[0,0]
    count=0
    one=0
    two=0
    record=[]
    record2 = []
    three=0
    for a in range(3,len(Nodes)-3):
        for b in range(3,len(Nodes)-3):
            Nodes[a][b].centercase = 0
            if Nodes[a][b].edgecase==1:
                for c in range(len(Nodes[a][b].fistring)):

                    if Nodes[a][b].fistring[c].edgecase==1:
                        #print(Nodes[a][b].fistring[c].edgecase)
                        record.append(Nodes[a][b].fistring[c])
                        one += 1
                for c in range(len(Nodes[a][b].secondring)):
                    if Nodes[a][b].secondring[c].edgecase==1:
                        #print(Nodes+[a][b].fistring[c].edgecase)
                        record2.append(Nodes[a][b].secondring[c])
                        two += 1
                """for c in range(len(Nodes[a][b].thirdring)):
                    if Nodes[a][b].thirdring[c].edgecase==1:
                        #print(Nodes+[a][b].fistring[c].edgecase)
                        three += 1"""
                #if len(record)>2:print(len(record))
                if one <=2 and one>0 and two<=2 :

                    if one==2:
                        x=record[0].posx-record[1].posx
                        y=record[0].posy-record[1].posy
                        xsqr=x*x
                        ysqr=y*y
                        magsqr=ysqr+xsqr


                        if magsqr<=1:
                            if two == 2:
                                x = record2[0].posx - record2[1].posx
                                y = record2[0].posy - record2[1].posy
                                xsqr = x * x
                                ysqr = y * y
                                magsqr = ysqr + xsqr
                                if magsqr <= 2:
                                    Nodes[a][b].centercase = 1
                                    count += 1
                            else:
                                Nodes[a][b].centercase = 1
                                count += 1
                    else:
                        if two == 2:
                            x = record2[0].posx - record2[1].posx
                            y = record2[0].posy - record2[1].posy
                            xsqr = x * x
                            ysqr = y * y
                            magsqr = ysqr + xsqr

                            if magsqr <= 3:
                                Nodes[a][b].centercase = 1
                                count+=1
                        else:
                            Nodes[a][b].centercase = 1
                            count += 1
                if timeout==0 and Nodes[a][b].centercase==1:
                    xdis=InitalCenterlast[0]-Nodes[a][b].posx
                    ydis=InitalCenterlast[1]-Nodes[a][b].posy
                    magsqr=xdis*xdis+ydis*ydis
                    if magsqr<=(Centbuffer+1):
                        trackedcenter+=1
                        suspectedCenter=[Nodes[a][b].posx,Nodes[a][b].posy]


            record2 = []
            record=[]
            one=0
            two=0
            three=0
    if timeout==0 and trackedcenter==1:
        if Centbuffer>0:
            x=(suspectedCenter[0]-InitalCenterlast[0])/Centbuffer
            y=(suspectedCenter[1]-InitalCenterlast[1])/Centbuffer
            for a in range(Centbuffer):
                InitalCenterlast[0]=int(round(suspectedCenter[0]+x))
                InitalCenterlast[1]=int(round(suspectedCenter[1]+y))
                InitalCenter.append(suspectedCenter)
            InitalCenterlast = suspectedCenter
        else:
            InitalCenterlast = suspectedCenter
            InitalCenter.append(suspectedCenter)
        BuffferRecrd.append(0)
        Centbuffer=0
    else:
        BuffferRecrd.append(1)
        Centbuffer+=1
        if Centbuffer>10:
            timeout=1

    return count
NxN=100
#a&b are revesed just because it works this way
Nodes = [[Node(b,a) for a in range(NxN)]for b in range(NxN)]
#Array for highlighting
Traces = [[0 for a in range(NxN-6)]for b in range(NxN-6)]
#calling def to find neighbors
for l in range(len(Nodes)):
    for m in range(len(Nodes)):
        FindNeighbors(Nodes[l][m],Nodes)
#used for our edge detection, a box slightly smaller that the original array
for l in range(3,len(Nodes)-3):
    for m in range(3,len(Nodes)-3):
        Findrings(Nodes[l][m],Nodes)


#Node for timing of frequencies
Listeningpost=Nodes[0][0]
Listeningpost.time=0
Listeningpost.frequecies=[]
Listeningpost.run=False
Listeningpost.priorstate="."


periods=[]
def Listen():
    val=0
    if Listeningpost.run==True:
        if (Listeningpost.state=="$" and Listeningpost.priorstate=="."):
            Listeningpost.frequecies.append(Listeningpost.time)
            periods.append(Listeningpost.time)
            Listeningpost.time=0
        Listeningpost.time+=1
    else:
        if (Listeningpost.state == "$" and Listeningpost.priorstate == "."):
            Listeningpost.run=True
    Listeningpost.priorstate=Listeningpost.state
    #print(Listeningpost.frequecies)
def set(i,l):
    TR = 50
    global upper_A
    global upper_R
    global Listeningpost
    global InitalCenter
    global Wavecount
    global avgspirals
    Listeningpost.frequecies=[]
    InitalCenter=[]
    Wavecount=[]
    avgspirals = 0
    global InitalCenterlast
    InitalCenterlast = [24, 23]
    global timeout
    timeout=0
    upper_A=i
    upper_R=l
    #circularbarrier(Nodes,Nodes[40][45],7)
    #circularbarrier(Nodes,Nodes[25][55],13)
    #circularbarrier(Nodes,Nodes[50][50],14)
    """for a in range(len(Nodes)):
        for b in range(len(Nodes[a])):
            Nodes[a][b].state="."
            Nodes[a][b].Current = 0
            Nodes[a][b].RefactoryTime = 0
            Nodes[a][b].ActiveTime = 0"""
    """Nodes[50][49].Current = TR
    Nodes[49][50].Current = TR
    Nodes[50][50].Current = TR"""
    for a in range(50):
        Nodes[a][49].state="$"
        Nodes[a][49].ActiveTime = upper_A
    for a in range(50):
        Nodes[a][50].state = "*"
        Nodes[a][50].RefactoryTime=upper_R

    """for a in range(len(Nodes[0])):
        Nodes[a][49].Current=TR"""

def circularbarrier(array,Node,dist):
    Neighdistreal=dist
    Neighdist=ceil(Neighdistreal)
    Neighdistsqr=Neighdistreal*Neighdistreal

    nx=Node.posx
    ny=Node.posy
    xplus=Neighdist
    xminus=Neighdist
    yplus=Neighdist
    yminus=Neighdist
    ystart =ny-Neighdist
    xstart=nx-Neighdist
    if nx<Neighdist:
        xminus=nx
        xstart=0
    if nx>(len(array)-Neighdist):
        xplus=len(array)-nx
    if ny<Neighdist:
        yminus=ny
        ystart=0
    if ny>(len(array[0])-Neighdist):
        yplus=len(array[0])-ny
    for a in range(xplus+xminus):
        for b in range(yplus+yminus):
            x=Nodes[a+xstart][b+ystart].posx-nx
            y=Nodes[a+xstart][b+ystart].posy-ny

            dist=x*x+y*y
            if (dist<=(Neighdistsqr)):
                Nodes[a+xstart][b+ystart].state='*'
                Nodes[a + xstart][b + ystart].RefactoryTime=60
def pulse():
    TR=6

    for a in range(len(Nodes[0])):
        Nodes[a][0].Current=TR
    """Nodes[25][24].Current = TR
    Nodes[24][23].Current = TR
    Nodes[25][23].Current = TR"""
set(1,1)
#Nodes=np.zeros((30,30),dtype=int)
np.set_printoptions(threshold=np.nan)

steps=1
periods=[]

fig = plt.figure()
data = np.zeros((NxN, NxN))
im = plt.imshow(data, cmap='gist_ncar', vmin=0, vmax=100)

avgspirals = 0
def sim(i):
    temp = np.zeros([NxN,NxN],dtype=int)
    global avgspirals
    for x in range(steps):

        for l in range(len(Nodes)):
            for m in range(len(Nodes)):
                ReAct(Nodes[l][m])

                temp[l, m] = 0
        FindEdge(Nodes)
        avgspirals+=Findcenter(Nodes)
        if i%13==0:
            Wavecount.append(avgspirals/10)
            #print(avgspirals / 10)
            avgspirals=0
            #pulse()
        for l in range(len(Nodes)):
            for m in range(len(Nodes)):
                Act(Nodes[l][m])


                if(Nodes[l][m].state=="$"):
                    temp[l,m]=12
                else:
                    if(Nodes[l][m].state=="*"): temp[l,m]= 6

                if(Nodes[l][m].edgecase==1):
                    temp[l,m]=20
                    if(Nodes[l][m].centercase==1):
                        temp[l,m]=70
                #temp[InitalCenterlast[0],InitalCenterlast[1]]=100

        #print(InitalCenter)
        Listen()

        im.set_data(temp)
def init():
    im.set_data(np.zeros((NxN, NxN)))
for a in range(5):
    print(a)
Writer = animation.writers['ffmpeg']
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
anim = animation.FuncAnimation(fig, sim, frames=700,init_func=init, interval=3000)

#anim.save("Powerpointvids/"+"small-line.mp4", writer=writer)
#print("Runs/Lower_A"+str(lower_A)+",Lower_R"+str(lower_R)+"Upper_A"+str(lower_A)+",Upper_R"+str(lower_R)+"T1.mp4 Done!")
#file format, change the numbers for appropriate lower bounds Lower_A1,Lower_R1"""

plt.show()
