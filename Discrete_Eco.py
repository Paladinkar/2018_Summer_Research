# A note, this has been fully retooled and now each class has it's own set of data to work off of.
# If other species will be affected we will pull the impact map of any interacting species.
# The impact score will basically tell us the number of species who's range overlap.
# For practical use, this should be a variable in some function to determine the affected species survival
from matplotlib import pyplot as plt
from matplotlib import animation
import random as rm
import numpy as np

from math import *
#import animal_wolf as wolf



#rm.seed(179874)
lower_A=1
upper_A=lower_A
lower_R=2
upper_R=lower_R

class Node:

    def __init__(self,x,y):
        self.Current = 0
        self.Delay=0
        self.Lifetime=0
        self.Neighbors=[]
        self.state = 0
        self.posx =x
        self.posy = y
        #Nodes can support herbavors with food. may need to be split later
        self.food=1
        #these following values are used for edge detection
        self.edgecontacts=[]
        self.fistring=[]
        self.secondring=[]
        self.thirdring=[]
        self.edgecase=0
        self.centercase=0
class species_plant:
    population = []
    priorpopulation = []
    growth_rate=20
    def __init__(self,x,y):
        #self.growth_rate = 20
        self.posx =x
        self.posy = y
        self.Neighbors = []
        self.edgecontacts=[]
        #Nodes can support herbavors with food. may need to be split later
        self.food=1

class species_animal_predator: #each animal will need a seperate class with an independent population map and properties, once this test is done, seperate files should be made.
    population = [] #empty array will be initalized the size of the species array, fill with zeros
    prior_population = []#previous step population, used to find the change in time
    impact_map=[]#each species should map their neighbors onto this impact map, this will be used to calculate spread and vaules for other species(defult impact should be 1)
    def __init__(self,x,y):
        self.mourning=0
        self.exists=0#possible cut, population map does this better
        self.lifespan = 10
        self.life = 0
        self.posx =x
        self.posy = y
        self.Neighbors = [] #this will be refrence which will store posx and posy as relavent data
        self.edgecontacts=[]
        #Nodes can support herbavors with food. may need to be split later
        self.food=1
        self.population=[]
        self.prior_population=[]
class species_animal_prey: #each animal will need a seperate class with an independent population map and properties, once this test is done, seperate files should be made.
    population = [] #empty array will be initalized the size of the species array, fill with zeros
    prior_population = []#previous step population, used to find the change in time
    impact_map=[]#each species should map their neighbors onto this impact map, this will be used to calculate spread and vaules for other species(defult impact should be 1)
    def __init__(self,x,y):
        self.mourning=0
        self.exists=0#possible cut, population map does this better
        self.lifespan = 5
        self.life = 0
        self.posx =x
        self.posy = y
        self.Neighbors = [] #this will be refrence which will store posx and posy as relavent data
        self.edgecontacts=[]
        #Nodes can support herbavors with food. may need to be split later
        self.food=1
        self.population=[]
        self.prior_population=[]

def initial_impact(species_array,species): #This is uses to initialize the impact of the species Run after species is initalized
    population=species.population#we need the class variable, they are shared amoung all class member so the first element works fine
    impact_map=species.impact_map
    for i in range(len(species_array)):
        for j in range(len(species_array[0])):
            if population[i][j]==1:
                for k in range(len(species_array[i][j].Neighbors)):
                    impact_map[species_array[i][j].Neighbors[k].posx][species_array[i][j].Neighbors[k].posy]+=1
    species.prior_population=population
    return

def update_impact(species_array,species):#one should exist for every species make files for this shit since they can affect different animals
    population=species_array.population
    initial_population=species_array.priorpopulation
    population_change=np.subtract(population,initial_population)
    for i in range(len(population_change)):
        for j in range(len(population_change[0])):
            if population_change[i][j]!=0:
                for k in range(len(species_array[i][j].Neighbors)):
                    species_array.impact_map[species_array[i][j].Neighbors.posx][species_array[i][j].Neighbors.posy]+=population_change[i][j]#this looks long, but names are nice
    return

def live_prey(prey_species_array,predator_species_array,plant_species_array):

    for i in range(len(prey_species_array)):
        for j in range(len(prey_species_array[0])):
            if prey_species_array.population[i][j]==0 and prey_species_array.impact[i][j]>1 and plant_species_array.food>.5:#reproduction criteria
                prey_species_array.population[i][j]=1
                prey_species_array[i][j].life=10
        else:
            if prey_species_array[i][j]==1:
                if prey_species_array[i][j].life==0:
                    prey_species_array.population[i][j]=0
                else:
                    if prey_species_array[i][j].life-predator_species_array.impact_map[i][j]/(plant_species_array[i][j].food*10)<5:#survival function here
                        prey_species_array.population[i][j] = 0
                    else:
                        prey_species_array[i][j].life -=1

    return


def live_predator(predator_species_array,prey_species_array):
    for i in range(len(predator_species_array)):
        for j in range(len(predator_species_array[0])):
            if predator_species_array.population[i][j]==0 and predator_species_array.impact_map[i][j]<1 and prey_species_array[i][j]<1:
                predator_species_array.population[i][j]=1
                predator_species_array[i][j].life=10
            else:
                if predator_species_array[i][j] == 1:
                    if predator_species_array.life==0:
                        predator_species_array.population[i][j]=0
                else:
                    if prey_species_array.impact<1 and predator_species_array[i][j].life<3:
                        predator_species_array.population[i][j]=0
                    else:
                        predator_species_array[i][j]-=1
    return
def live_plant(plant_species_array,prey_species_array):
    for i in range(len(plant_species_array)):
        for j in range(len(plant_species_array[0])):
            plant_species_array[i][j].food = np.arctan(plant_species_array[i][j].food+plant_species_array[0][0].growth_rate-prey_species_array[0][0].impact_map[i][j])/(np.pi/2)

def ReAct(Node): #Change of state and cool-down periods
    if (Node.state==2):
        if (Node.Delay==0):
            Node.state=0
            Node.Current=0
        else:
            Node.Delay -=1



    else:
        if (Node.state==0):
            if (Node.Current>1):
                Node.state=1
                Node.Lifetime=rm.randint(lower_A,upper_A)
            #Node.Lifetime=Activ
                Node.Current=0
            else:
                if (Node.Current>0):
                    Node.Current -=1



        else:
            if (Node.state==1):
                if(Node.Lifetime==0):
                    Node.state=2
                    Node.Delay=rm.randint(lower_R,upper_R)
                    #Node.Delay=Refac
                else:
                    Node.Lifetime -=1
    return
def Act(Node):
    if (Node.state==1):
        for x in range(len(Node.Neighbors)):
            if (Node.Neighbors[x].state==0):
                Node.Neighbors[x].Current+= 1
    return

def FindNeighbors(Node,array):
    Neighdistreal=1.5
    Neighdist=ceil(Neighdistreal)
    Neighdistsqr=Neighdistreal*Neighdistreal
    nx=Node.posx
    ny=Node.posy
    xplus=Neighdist
    xminus=Neighdist
    yplus=Neighdist
    yminus=Neighdist
    ystart =ny-Neighdist
    xstart=nx-Neighdist
    #print("bong")
    if nx<Neighdist:
        xminus=nx
        xstart=0
    if nx>(len(array)-Neighdist):
        xplus=len(array)-nx
    if ny<Neighdist:
        yminus=ny
        ystart=0
    if ny>(len(array[0])-Neighdist):
        yplus=len(array[0])-ny

    for a in range(xplus+xminus):
        for b in range(yplus+yminus):
            x=Nodes[a+xstart][b+ystart].posx-nx
            y=Nodes[a+xstart][b+ystart].posy-ny

            dist=x*x+y*y
            if (dist<=(Neighdistsqr) and dist!=0):
                Node.Neighbors.append(Nodes[a+xstart][b+ystart])
            #this next bit is for edge detection
            if (dist<=(1) and dist!=0):
                Node.edgecontacts.append(Nodes[a+xstart][b+ystart])
            #print(str(xstart) + "|" + str(ystart))
            #print(str(nx)+"/"+str(ny))
    return
def Findrings(Node,Nodes):
    x=Node.posx-1
    y=Node.posy-1
    #start here, the we go around
    Node.fistring.append(Nodes[x][y])
    for a in range(2):
        x+=1
        Node.fistring.append(Nodes[x][y])
    for a in range(2):
        y += 1
        Node.fistring.append(Nodes[x][y])
    for a in range(2):
        x -= 1
        Node.fistring.append(Nodes[x][y])
    for a in range(1):
        y -= 1
        Node.fistring.append(Nodes[x][y])
    #second ring
    x=Node.posx-2
    y=Node.posy-2
    Node.secondring.append(Nodes[x][y])
    for a in range(4):
        x += 1
        Node.secondring.append(Nodes[x][y])
    for a in range(4):
        y += 1
        Node.secondring.append(Nodes[x][y])
    for a in range(4):
        x -= 1
        Node.secondring.append(Nodes[x][y])
    for a in range(3):
        y -= 1
        Node.secondring.append(Nodes[x][y])
    #third ring
    x=Node.posx-3
    y=Node.posy-3
    Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        x += 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        y += 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(6):
        x -= 1
        Node.thirdring.append(Nodes[x][y])
    for a in range(5):
        y -= 1
        Node.thirdring.append(Nodes[x][y])


    return

NxN=50
#a&b are revesed just because it works this way
Nodes = [[Node(b,a) for a in range(NxN)]for b in range(NxN)]

Predator = [[species_animal_predator(b,a) for a in range(NxN)]for b in range(NxN)]
Prey = [[species_animal_prey(b,a) for a in range(NxN)]for b in range(NxN)]
Plant = [[species_plant(b,a) for a in range(NxN)]for b in range(NxN)]

species_animal_prey.population=[[0 for a in range(NxN)]for b in range(NxN)]#probably take this shit out of the class
species_animal_predator.population=[[0 for a in range(NxN)]for b in range(NxN)]
species_animal_prey.impact_map=[[0 for a in range(NxN)]for b in range(NxN)]
species_animal_predator.impact_map=[[0 for a in range(NxN)]for b in range(NxN)]


#calling def to find neighbors
for l in range(len(Nodes)):
    for m in range(len(Nodes)):
        FindNeighbors(Predator[l][m],Predator)
        FindNeighbors(Prey[l][m], Prey)
        FindNeighbors(Nodes[l][m], Nodes)

#used for our edge detection, a box slightly smaller that the original array
for l in range(3,len(Nodes)-3):
    for m in range(3,len(Nodes)-3):
        Findrings(Nodes[l][m],Nodes)
#Species


#Node for timing of frequencies
Listeningpost=Nodes[0][0]
Listeningpost.time=0
Listeningpost.frequecies=[]
Listeningpost.run=False
Listeningpost.priorstate=0


periods=[]
def set(i,l):
    TR = 6
    global upper_A
    global upper_R
    global Listeningpost
    global InitalCenter
    global Wavecount
    global avgspirals
    global  Centbuffer
    Centbuffer=0
    Listeningpost.frequecies=[]
    InitalCenter=[]
    Wavecount=[]
    avgspirals = 0
    global InitalCenterlast
    InitalCenterlast = [24, 23]
    global timeout
    timeout=0
    upper_A=i
    upper_R=l
    for a in range(len(Nodes)):
        for b in range(len(Nodes[a])):
            Nodes[a][b].state=0
            Nodes[a][b].Current = 0
            Nodes[a][b].Delay = 0
            Nodes[a][b].Lifetime = 0
    species_animal_prey.population[10][24]=1
    species_animal_prey.population[9][23]=1
    species_animal_prey.population[10][23]=1

    for a in range(24):
        Nodes[a][24].Current=TR
    for a in range(len(Nodes)):
        Nodes[a][25].state = 2
        Nodes[a][25].Delay=4

set(2,3)
initial_impact(Prey,species_animal_prey)
initial_impact(Predator,species_animal_predator)
steps=1
periods=[]

fig = plt.figure()
data = np.zeros((NxN, NxN))
im = plt.imshow(data, cmap='gist_ncar', vmin=0, vmax=100)
print(Nodes[20][20].Neighbors[0].posy)
avgspirals = 0
### NOTICE: temp is responsible for color, x & y are swaped on the graph
def sim(i):
    temp = np.zeros([NxN,NxN],dtype=int)
    for x in range(steps):

        for l in range(len(Nodes)):
            for m in range(len(Nodes)):
                ReAct(Nodes[l][m])
                live_plant(Plant,Prey)
                live_predator(Predator,Prey)
                live_prey(Prey,Predator,Plant)
                temp[l, m] = 0


        for l in range(len(Nodes)):
            for m in range(len(Nodes)):
                Act(Nodes[l][m])


                if(Nodes[l][m].state==1):
                    temp[l,m]=12
                else:
                    if(Nodes[l][m].state==2): temp[l,m]= 6

                if(Nodes[l][m].edgecase==1):
                    temp[l,m]=20
                    if(Nodes[l][m].centercase==1):
                        temp[l,m]=70


        #print(InitalCenter)


        im.set_data(temp)

#just getting velocities & average velocites


print("Starting sims")

UpperA=2
UpperR=3

SimSteps=1000
SimPages=100

def init():
    im.set_data(np.zeros((NxN, NxN)))


set(UpperA,UpperR)
xper = [a for a in range(len(periods))]
#plt.plot(xper,periods)
anim = animation.FuncAnimation(fig, sim, init_func=init, frames=2000,
                               interval=30)

plt.show()
